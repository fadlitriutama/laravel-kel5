<div class="col-md-3">
    <ul class="list-group list-group-flush">
        <li class="list-group-item">
            <a href="">
                <div class="side-img">
                    <img src="/images/testi4.png" class="w-80 photo-profile mr-1" alt="">
                    <p class="d-inline">{{Auth::user()->name }}</p>
                </div>
            </a>
        </li>
        <li class="list-group-item">
            <a href="">
                <div class="side-img d-inline">
                    <img src="/images/ico-news.png" class="w-80 icon" alt="">
                    <p class="d-inline">News</p>
                </div>
            </a>
        </li>
        <li class="list-group-item">
            <a href="">
                <div class="side-img d-inline">
                    <img src="/images/ico-yt.png" class="w-80 icon" alt="">
                    <p class="d-inline">Videos</p>
                </div>
            </a>
        </li>
        <li class="list-group-item">
            <a href="">
                <div class="side-img d-inline">
                    <img src="/images/ico-market.png" class="w-80 icon" alt="">
                    <p class="d-inline">Market</p>
                </div>
            </a>
        </li>
        <li class="list-group-item">
            <a href="">
                <div class="side-img d-inline">
                    <img src="/images/ico-event.png" class="w-80 icon" alt="">
                    <p class="d-inline">Events</p>
                    </di>
            </a>
        </li>
        <li class="list-group-item">
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                <div class=" side-img d-inline">
                    <img src="/images/ico-event.png" class="w-80 icon" alt="">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    <span class="hide-menu">Logout</span>
            </a>
        </li>
    </ul>
</div>