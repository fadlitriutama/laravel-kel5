@include('include.header')
<div class="container-fluid" id="containerHome">
    <!-- navbar -->
    @include('include.navbar')
    <!-- page content -->
    <div class="container-fluid" id="page-home">
        <div class="row">
            @include('include.sidebar')
            <div class="col-md-6">
                <div class="card card-home">
                    <div class="card-body">
                        <h5>Post</h5>
                        <form action="">
                            <div class="form-group">
                                <textarea name="" id="editor2" class="form-control"></textarea>
                            </div>
                            <button class="btn btn-info btn-md btn-block">Post</button>
                        </form>
                    </div>
                </div>
                <div class="card mt-5">
                    <div class="card-body">
                        <div class="border post mt-2">
                            <img src="/images/testi4.png" class="w-80 photo-profile ml-2 mt-2" alt="">
                            <p class="d-inline mt-1 ml-2">Andriansyah Saputra</p>
                            <p class="ml-2">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deleniti quas voluptatem obcaecati magni deserunt fugiat amet illum doloremque officiis itaque numquam culpa ea enim illo quos veniam accusamus quae, fugit, praesentium pariatur reprehenderit rerum architecto vero atque. Nemo odit aliquid quibusdam ipsam, ipsa animi autem? Exercitationem, sed! A, porro alias?</p>
                            <div class="row mb-1">
                                <div class="d-inline ml-4">
                                    <a href="">Like</a>
                                    <a href="">dislike</a>
                                </div>
                            </div>
                            <p class="ml-2">Beri Komentar</p>
                            <textarea name="" class="form-control ml-2" style="width: 550px;"></textarea>
                            <div class="komentar mb-2 ml-2">
                                <p>Komentar</p>
                                <img src="/images/testi4.png" class="w-80 photo-profile" alt="">
                                <p class="d-inline">haha ga lucu</p>
                            </div>
                        </div>

                        <div class="border post mt-2">
                            <img src="/images/testi4.png" class="w-80 photo-profile ml-2 mt-2" alt="">
                            <p class="d-inline mt-1 ml-2">Andriansyah Saputra</p>
                            <p class="ml-2">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deleniti quas voluptatem obcaecati magni
                                deserunt fugiat amet illum doloremque officiis itaque numquam culpa ea enim illo quos veniam accusamus quae,
                                fugit, praesentium pariatur reprehenderit rerum architecto vero atque. Nemo odit aliquid quibusdam ipsam, ipsa
                                animi autem? Exercitationem, sed! A, porro alias?</p>
                            <div class="row mb-1">
                                <div class="d-inline ml-4">
                                    <a href="">Like</a>
                                    <a href="">dislike</a>
                                </div>
                            </div>
                            <p class="ml-2">Beri Komentar</p>
                            <textarea name="" class="form-control ml-2" style="width: 550px;"></textarea>
                            <div class="komentar mb-2 ml-2">
                                <p>Komentar</p>
                                <img src="/images/testi4.png" class="w-80 photo-profile" alt="">
                                <p class="d-inline">haha ga lucu</p>
                            </div>
                        </div>

                        <div class="border post mt-2">
                            <img src="/images/testi4.png" class="w-80 photo-profile ml-2 mt-2" alt="">
                            <p class="d-inline mt-1 ml-2">Andriansyah Saputra</p>
                            <p class="ml-2">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Deleniti quas voluptatem obcaecati magni
                                deserunt fugiat amet illum doloremque officiis itaque numquam culpa ea enim illo quos veniam accusamus quae,
                                fugit, praesentium pariatur reprehenderit rerum architecto vero atque. Nemo odit aliquid quibusdam ipsam, ipsa
                                animi autem? Exercitationem, sed! A, porro alias?</p>
                            <div class="row mb-1">
                                <div class="d-inline ml-4">
                                    <a href="">Like</a>
                                    <a href="">dislike</a>
                                </div>
                            </div>
                            <p class="ml-2">Beri Komentar</p>
                            <textarea name="" class="form-control ml-2" style="width: 550px;"></textarea>
                            <div class="komentar mb-2 ml-2">
                                <p>Komentar</p>
                                <img src="/images/testi4.png" class="w-80 photo-profile" alt="">
                                <p class="d-inline">haha ga lucu</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card mt-3">
                    <div class="card-body">
                        <h5>New Upcoming Movie</h5>
                        <div class="owl-carousel owl-theme" id="owl-car1">
                            <div class="item">
                                <img src="/images/avangers.jpg" class="w-100 baner-film" alt="">
                            </div>
                            <div class="item">
                                <img src="/images/harrypottret.jpg" class="w-100 baner-film" alt="">
                            </div>
                            <div class="item">
                                <img src="/images/ironman.jpg" class="w-100 baner-film" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mt-3">
                    <div class="card-body">
                        <h5>Product Hightlight</h5>
                        <div class="owl-carousel owl-theme" id="owl-car2">
                            <div class="item">
                                <img src="/images/laptop.jpg" class="w-100 baner-film" alt="">
                            </div>
                            <div class="item">
                                <img src="/images/bajukeren.jpg" class="w-100 baner-film" alt="">
                            </div>
                            <div class="item">
                                <img src="/images/hpker.jpg" class="w-100 baner-film" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('include.footer')